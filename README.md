# How Are You?

[![Pipeline](https://gitlab.com/kamilaalifia/story-6/badges/master/pipeline.svg)](https://gitlab.com/kamilaalifia/story-6/pipelines)
[![coverage report](https://gitlab.com/kamilaalifia/story-6/badges/master/coverage.svg)](https://gitlab.com/kamilaalifia/story-6/commits/master)


## URL

[https://status-kamila.herokuapp.com](https://status-kamila.herokuapp.com)
