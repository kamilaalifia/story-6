from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    status = forms.CharField(widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder':'How is your day?'}))

    class Meta:
        model = Status
        fields = ['status']