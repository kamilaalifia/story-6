from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from django.conf import settings
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import *
from .models import *

class Story6UnitTest(TestCase):
    def test_story_6_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_url_404_error(self):
        response = Client().get('/wek/')
        self.assertEqual(response.status_code, 404)

    def test_story_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home, delete)

    def test_status_input_redirects_to_homepage(self):
        response = Client().post('/', {'status':'test'})
        self.assertRedirects(response, '/')

    def test_delete_status(self):
        obj = Status(status="test")
        obj.save()
        response = self.client.get("/delete/")
        self.assertEqual(Status.objects.all().count(),0)
    
    def test_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_profilepage(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)
    
    def test_using_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')
    
    def test_profilepage_has_name(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('h1', html_response)

    def test_profilepage_has_description(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('description', html_response)

    def test_profilepage_has_photo(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('img', html_response)

    def test_library_is_exist(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_library_page(self):
        found = resolve('/library/')
        self.assertEqual(found.func, library)

    def test_using_library_template(self):
        response = Client().get('/library/')
        self.assertTemplateUsed(response, 'library.html')

    def test_library_page_has_title(self):
        response = Client().get('/library/')
        html_response = response.content.decode('utf8')
        self.assertIn('h1', html_response)

class StatusFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.browser
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        time.sleep(2)
        status.send_keys('test')

        # submitting the form
        time.sleep(2)
        submit.send_keys(Keys.RETURN)

        time.sleep(2)
        assert 'test' in selenium.page_source

    def test_profilepage(self):
        selenium = self.browser
        selenium.get(self.live_server_url + '/profile/')
        
        title = selenium.find_element_by_id('name')
        self.assertEquals(title.text, 'kamila alifia.')

        accordion = selenium.find_element_by_id('accordion')
        time.sleep(2)
        accordion.click()

        panel = selenium.find_element_by_tag_name('h4')
        time.sleep(2)
        self.assertEquals(panel.text, "Activities")