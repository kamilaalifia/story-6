from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import Context, loader
from .forms import StatusForm
from .models import Status
from django.contrib.auth import authenticate, login as auth_SignIn, logout as auth_SignOut

def home(request):
	form = StatusForm(request.POST or None)
	response = {}
	if(request.method == "POST" and form.is_valid()):
		if (form.is_valid()):
			status = request.POST.get("status")
			created_at = request.POST.get("created_at")
			Status.objects.create(status=status, created_at=created_at)
			return redirect('/')
	else:
		allStatus = Status.objects.all().order_by('-id')
		response['form'] = form
		response['allStatus'] = allStatus
		return render(request, 'status.html', response)

def delete(request):
	allStatus = Status.objects.all().delete()
	return redirect('/')

def profile(request):
	return render(request, 'profile.html')

def library(request):
	return render(request, 'library.html')