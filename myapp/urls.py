from django.urls import path
from . import views

app_name = 'myapp'

urlpatterns = [
    path('', views.home, name='home'),
    path('delete/', views.delete, name='delete'),
    path('profile/', views.profile, name='profile'),
    path('library/', views.library, name='library'),
]