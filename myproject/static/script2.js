$(document).ready(function(){
  var booktitle = "Programming"
  console.log("search", booktitle)
  $.ajax({
      method: "GET",
      url: "https://www.googleapis.com/books/v1/volumes?q="+booktitle,
      success: function(show){
      $("table").empty();
      $("table").append(
          '<thead>' +
          '<tr>' +
              '<th scope="col">Book Cover</th>' +
              '<th scope="col">Book Title</th>' +
              '<th scope="col">Book Author</th>' +
              '<th scope="col">Book Description</th>' +
          '</tr>'+
          '</thead>'
      );
      for(i = 0; i < show.items.length ; i++){
          $("table").append(
          "<tbody>" +
              "<tr>" +
              "<th scope='row'>" + "<img src= '"+ show.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
              "<td>" + show.items[i].volumeInfo.title + "</td>" +
              "<td>" + show.items[i].volumeInfo.authors + "</td>" +
              "<td>" + show.items[i].volumeInfo.description + "</td>" +
              "</tr>" +
          "</tbody>"
          );
      }
      }
  }) 
  
  $("button").click(
      function(){
        var keyword = $("#field").val()
        console.log("run", keyword)
        $.ajax({
          method: "GET",
          url: "https://www.googleapis.com/books/v1/volumes?q="+keyword,
          success: function(show){
            $("table").empty();
            $("table").append(
              '<thead>' +
                '<tr>' +
                  '<th scope="col">Book Picture</th>' +
                  '<th scope="col">Book Title</th>' +
                  '<th scope="col">Book Author</th>' +
                  '<th scope="col">Book Description</th>' +
                '</tr>'+
              '</thead>'
            );
            for(i = 0; i < show.items.length ; i++){
              $("table").append(
                "<tbody>" +
                  "<tr>" +
                    "<th scope='row'>" + "<img src= '"+ show.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                    "<td>" + show.items[i].volumeInfo.title + "</td>" +
                    "<td>" + show.items[i].volumeInfo.authors + "</td>" +
                    "<td>" + show.items[i].volumeInfo.description + "</td>" +
                  "</tr>" +
                "</tbody>"
              );
            }
          }
        })
      }
    );
  })
  
  $("input").change(
    function(){
      console.log("run run")
    }
  );
  