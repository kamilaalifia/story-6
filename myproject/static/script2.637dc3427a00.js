$(document).ready(function(){
    $("button").click(
      function(){
        var keyword = $("#field").val()
        console.log("run", keyword)
        $.ajax({
          method: "GET",
          url: "https://www.googleapis.com/books/v1/volumes?q="+keyword,
          success: function(result){
            $("table").empty();
            $("table").append(
              '<thead>' +
                '<tr>' +
                  '<th scope="col">Book Picture</th>' +
                  '<th scope="col">Book Title</th>' +
                  '<th scope="col">Book Author</th>' +
                  '<th scope="col">Book Description</th>' +
                '</tr>'+
              '</thead>'
            );
            for(i = 0; i < result.items.length ; i++){
              $("table").append(
                "<tbody>" +
                  "<tr>" +
                    "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                    "<td>" + result.items[i].volumeInfo.title + "</td>" +
                    "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                    "<td>" + result.items[i].volumeInfo.description + "</td>" +
                  "</tr>" +
                "</tbody>"
              );
            }
          }
        })
      }
    );
  })
  
  $("input").change(
    function(){
      console.log("run run")
    }
  );
  